# Markdown to PDF

**md2pdf** converts a markdown file to pdf.
You can set a css stylesheet.
Because of Maud you need to install nightly.
You can run the debug version using the below.

````
cargo +nightly run -- -i pilatesscoliosis.md -o pilatesscoliosis.pdf --no_html -w --css style.css > /dev/null 2>&1

````
