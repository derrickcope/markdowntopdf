#![feature(proc_macro_hygiene)]
extern crate clap;
use clap::{clap_app, crate_version};
use maud::html;
use pulldown_cmark::{html::push_html, Event, Parser};
use std::fs::{remove_file, File};
use std::io::prelude::*;
// use std::io::BufWriter;
use wkhtmltopdf::*;

fn wrap_html(s: &str, css: Option<&str>) -> String {
    let res = html! {
    (maud::DOCTYPE)
    html {
        head {
            meta charset="utf-8";
            @if let Some(s) = css {
                link rel="stylesheet" type="text/css" href=(s) {}
            }
        }
        body {
            (maud::PreEscaped(s))
                footer { img src="http://www.pilatesshanghai.com/handoutfooter.png" {}

            }
        }
    }
    };
    res.into_string()
}

fn main() {
    let clap = clap_app!(md2pdf =>
    (version:crate_version!())
    (author:"derrick cope <derrick@thecopes.me>")
    (about:"convert markdown file to pdf")
    (@arg input: -i +required +takes_value "sets the input file")
    (@arg output: -o +required +takes_value "set the output file")
    (@arg wrap: -w --wrap_html "Wrap in html")
    (@arg html: -H --no_html "delete temp.html file")
    (@arg event: -e "Print events")
    (@arg css: -c --css +takes_value "Link to css")
    )
    .get_matches();

    // println!("Input = {:?}", clap.value_of("input").unwrap());

    let infile =
        std::fs::read_to_string(clap.value_of("input").unwrap()).expect("could not read file");

    let ps = Parser::new(&infile);

    let ps: Vec<Event> = ps.into_iter().collect();
    if clap.is_present("event") {
        for p in &ps {
            println!("{:?}", p);
        }
    }

    // println!("{:?}", ps);

    let mut res = String::new();
    push_html(&mut res, ps.into_iter());

    if clap.is_present("wrap") {
        res = wrap_html(&res, clap.value_of("css"));
        // println!("{:?}", res);
    }

    // println!("{}", res);
    let mut buffer = File::create("temp.html").unwrap();
    buffer.write(res.as_bytes()).unwrap();

    let mut pdf_app = PdfApplication::new().expect("failed to instantiate pdf application");
    let mut pdf_out = pdf_app
        .builder()
        .orientation(Orientation::Portrait)
        .page_size(PageSize::A4)
        .margin(Size::Inches(1))
        .title("temp pdf")
        // .build_from_html(res)
        .build_from_path("temp.html")
        .expect("failed to create pdf");

    pdf_out
        .save(clap.value_of("output").unwrap())
        .expect("failed to save pdf");

    if clap.is_present("html") {
        remove_file("temp.html").expect("could not remove temp file");
    }
}
